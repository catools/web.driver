package org.catools.web.enums;

public enum CBrowser {
    FIREFOX,
    CHROME;

    public boolean isChrome() {
        return CHROME.equals(this);
    }

    public boolean isFirefox() {
        return FIREFOX.equals(this);
    }
}
